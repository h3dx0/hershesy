from django.db import models


# Create your models here.
class Chocolates(models.Model):
    class Meta:
        verbose_name_plural = 'Chocolates'
    austria = models.CharField(max_length=50,default=11.9)
    switzerland = models.CharField(max_length=50,default=8.8)
    mexico = models.CharField(max_length=50,default=0.75)
    ghana = models.CharField(default="835,000", max_length=50)
    ivory_coast = models.CharField(default="1,449,000", max_length=50)
    tabasco = models.CharField(default="67", max_length=50)
    chiapas = models.CharField(default="31", max_length=50)
    left_ship = models.CharField(default="410", max_length=50)
    right_ship = models.CharField(default="410", max_length=50)
    sensory = models.CharField(default="18", max_length=50)
    escape_seekers = models.CharField(default="21",max_length=50)
    price_seekers = models.CharField(default="25", max_length=50)
    health_conscious = models.CharField(default="21", max_length=50)
    uninvolved = models.CharField(default="21", max_length=50)
    ferrero_rocher = models.CharField(default="24", max_length=50)
    mars = models.CharField(default=22, max_length=50)
    hersheys = models.CharField(default=17, max_length=50)
    nestle = models.CharField(default=14, max_length=50)
    grebon = models.CharField(default=6, max_length=50)
    other = models.CharField(default=17, max_length=50)
    presentation_consumed1 = models.CharField(max_length=50,default=35)
    presentation_consumed2 = models.CharField(max_length=50,default=23)
    presentation_consumed3 = models.CharField(max_length=50,default=15)
    almost = models.CharField(max_length=50,default=7)
    out_of = models.CharField(max_length=50,default=10)
    HBD = models.CharField(max_length=50,default=39)
    valentines_day = models.CharField(max_length=50,default=32)
    mothers_day = models.CharField(max_length=50,default=30)
    christmas_day = models.CharField(max_length=50,default=23)
    anniversary = models.CharField(max_length=50,default=22)
    childrens_day = models.CharField(max_length=50,default=16)
    without_a_specific_reason = models.CharField(max_length=50,default=13)
    fathers_day = models.CharField(max_length=50,default=22)
    teachers_day = models.CharField(max_length=50,default=9)
    unique_moment = models.CharField(max_length=50,default=8)
    halloween = models.CharField(max_length=50,default=5)


class BarConsumers(models.Model):
    class Meta:
        verbose_name_plural = 'BarConsumers'
    frequency_of_consumption_one = models.CharField(default="3 times per week 27", max_length=50)
    frequency_of_consumption_two = models.CharField(default="Once a day 25", max_length=50)
    flavor = models.CharField(max_length=50,default=27)
    who_was_you_just_me = models.CharField(max_length=50,default="27")
    who_was_you_partner = models.CharField(max_length=50,default="25")
    age = models.CharField(max_length=50,default="39")
    gender_male = models.CharField(max_length=50,default="52")
    gender_female = models.CharField(max_length=50,default="48")
    sustitute_product = models.CharField(max_length=50,default="30")
    place_consumption = models.CharField(max_length=50,default="60")
    way_to_consume_chocolate = models.CharField(max_length=50,default="83")
    activity = models.CharField(max_length=50,default="21")
    city = models.CharField(max_length=50,default="58")
    type = models.CharField(max_length=50,default="53")
    place_purchase = models.CharField(max_length=50,default="53")
    sel = models.CharField(max_length=50,default="42")
    kind_pack = models.CharField(max_length=50,default="63")
    average_spent_per_month = models.CharField(max_length=50,default="122.2")


class BitesConsumers(models.Model):
    class Meta:
        verbose_name_plural = 'BitesConsumers'
    frequency_of_consumption = models.CharField(max_length=50,default=27)
    flavor = models.CharField(max_length=50,default=33)
    who_was_you_just_me = models.CharField(max_length=50,default=84)
    age = models.CharField(max_length=50,default=41)
    gender_male = models.CharField(max_length=50,default=57)
    gender_female = models.CharField(max_length=50,default=43)
    ice_cream = models.CharField(max_length=50,default=27)
    almods = models.CharField(max_length=50,default=26)
    place_consumption = models.CharField(max_length=50,default=57)
    way_to_consume_chocolate = models.CharField(max_length=50,default=69)
    at_work = models.CharField(max_length=50,default=15)
    watching_tv = models.CharField(max_length=50,default=14)
    city = models.CharField(max_length=50,default=60)
    solid = models.CharField(max_length=50,default=35)
    insertion = models.CharField(max_length=50,default=23)
    place_purchase = models.CharField(max_length=50,default=23)
    sel = models.CharField(max_length=50,default=40)
    kind_pack = models.CharField(max_length=50,default=50)
    average_spent_per_month = models.CharField(max_length=50,default=120.7)


class KissesConsumers(models.Model):
    class Meta:
        verbose_name_plural = 'KissesConsumers'
    frequency_of_consumption = models.CharField(max_length=50,default=26)
    flavor = models.CharField(max_length=50,default=46)
    who_was_you = models.CharField(max_length=50,default=26)
    age = models.CharField(max_length=50,default=39)
    gender_male = models.CharField(max_length=50,default=51)
    gender_female = models.CharField(max_length=50,default=49)
    sustitute_product = models.CharField(max_length=50,default=27)
    place_consumption = models.CharField(max_length=50,default=63)
    way_to_consume_chocolate = models.CharField(max_length=50,default=73)
    activity = models.CharField(max_length=50,default=19)
    city = models.CharField(max_length=50,default=58)
    type = models.CharField(max_length=50,default=49)
    place_purchase = models.CharField(max_length=50,default=52)
    sel = models.CharField(max_length=50,default=41)
    kind_pack = models.CharField(max_length=50,default=40)
    average_spent_per_month = models.CharField(max_length=50,default=110.7)


class SwotOfHersheys(models.Model):
    class Meta:
        verbose_name_plural = 'SwotOfHersheys'

    strengths = models.TextField(default="Kisses is the brand driving growth in value Hershey´s is associated with moments of joy.Kisses is perceived as a brand good for sharing Kisses is the 2nd brand that consumers look for gifting. ", max_length=600)
    WEAKNESSES = models.TextField(default="Ferrero is the leader of the category.Hershey´s is the 3rd place. Hershey´s bar is perceived as a more expensive chocolate. ", max_length=600)
    OPPORTUNITIES = models.TextField(default="Capitalize a younger audience. Chocolate and Candy are in the top categories that drive sales growth during Xmas season. ", max_length=600)
    THREATS = models.TextField(default=" The increase awareness of health issues.obesity and diabetes. 58% consider Ferrero Rocher as their 1st option when buying a chocolate for gifting. ", max_length=600)


class SpicyCandy(models.Model):
    class Meta:
        verbose_name_plural = 'SpicyCandy'
    since = models.CharField(max_length=50, default="since 2008")
    annyally_percentage = models.CharField(max_length=50,default=2)
    annyally_mxn = models.CharField(max_length=50,default=38)
    year1 = models.IntegerField(default=2016)
    year2 = models.IntegerField(default=2017)
    year3 = models.IntegerField(default=2018)
    year4 = models.IntegerField(default=2019)
    year5 = models.IntegerField(default=2020)
    year6 = models.IntegerField(default=2021)
    year7 = models.IntegerField(default=2022)
    year8 = models.IntegerField(default=2023)
    teens = models.CharField(max_length=50,default=7.46 )
    thirtysix_fortyfive = models.CharField(max_length=50,default=12.34)
    convenience = models.CharField(max_length=50,default=85)
    buying_units = models.CharField(max_length=50,default=50)
    pay_between = models.CharField(default="5-9", max_length=50)
    frequency = models.CharField(max_length=50,default=5.4)


class SpicyCandySegment(models.Model):
    class Meta:
        verbose_name_plural = 'SpicyCandySegment'
    Candy = models.CharField(max_length=50,default=10)
    gummies = models.CharField(max_length=50,default=16)
    lollipop = models.CharField(max_length=50,default=39)
    pulp = models.CharField(max_length=50,default=3)
    powder = models.CharField(max_length=50,default=32)


class Consuption(models.Model):
    class Meta:
        verbose_name_plural = 'Consuption'
    pelon_most_frequent = models.CharField(max_length=50,default=27)
    pelon_regular_consuption = models.CharField(max_length=50,default=55)
    pelon_trial = models.CharField(max_length=50,default=82)
    pelon_total = models.CharField(max_length=50,default=95)

    swinkles_most_frequent = models.CharField(max_length=50,default=25)
    swinkles_regular_consuption = models.CharField(max_length=50,default=55)
    swinkles_trial = models.CharField(max_length=50,default=76)
    swinkles_total = models.CharField(max_length=50,default=93)

    lucas_most_frequent = models.CharField(max_length=50,default=33)
    lucas_regular_consuption = models.CharField(max_length=50,default=57)
    lucas_trial = models.CharField(max_length=50,default=79)
    lucas_total = models.CharField(max_length=50,default=97)

    roca_most_frequent = models.CharField(max_length=50,default=31)
    rocka_regular_consuption = models.CharField(max_length=50,default=58)
    rocka_trial = models.CharField(max_length=50,default=79)
    rocka_total = models.CharField(max_length=50,default=95)


class Milk(models.Model):
    class Meta:
        verbose_name_plural = 'Milk'
    litters_year = models.IntegerField(default=110)
    litters_fao_recommends = models.CharField(max_length=50,default="150")
    mexico = models.CharField(default="265,000", max_length=50)
    usa = models.CharField(default="496,000", max_length=50)
    WORLD_TOTAL = models.CharField(max_length=50,default=2.5)
    lala = models.CharField(max_length=50,default=46)
    alpura = models.CharField(max_length=50,default=22)
    jalisco = models.CharField(default="2,157,000,000", max_length=50)
    durango = models.CharField(default="1,142,000,000", max_length=50)
    coahuila = models.CharField(default="1,380,000,000", max_length=50)
    year = models.IntegerField(default=2015)
    liters_milk = models.CharField(default="2,970,000,000", max_length=50)
    sales_mix1 = models.CharField(max_length=50,default=56)
    sales_mix2 = models.CharField(max_length=50,default=21)
    sales_mix3 = models.CharField(max_length=50,default=12)
    top_manifactures_santa_clara = models.CharField(max_length=50,default=25)
    top_manifactures_lala = models.CharField(max_length=50,default=30)
    top_manifactures_alpura = models.CharField(max_length=50,default=27)
    top_manifactures_hersheys = models.CharField(max_length=50,default=10)
    top_manifactures_other = models.CharField(max_length=50,default=8)


class HysMilkKids(models.Model):
    class Meta:
        verbose_name_plural = 'HysMilkKids'
    hys_frequent = models.CharField(max_length=50,default="17")
    hys_regular_consuption = models.CharField(max_length=50, default="32")
    hys_trial = models.CharField(max_length=50,default="67")
    hys_total = models.CharField(max_length=50,default="87")

    alpura_frequent = models.CharField(max_length=50,default="7")
    alpura_regular_consuption = models.CharField(max_length=50,default="27")
    alpura_trial = models.CharField(max_length=50,default="45")
    alpura_total = models.CharField(max_length=50,default="74")

    santa_clara_frequent = models.CharField(max_length=50,default="6")
    santa_clara_regular_consuption = models.CharField(max_length=50,default="12")
    santa_clara_trial = models.CharField(max_length=50,default="32")
    santa_clara_total = models.CharField(max_length=50,default="55")

    lala_frequent = models.CharField(max_length=50,default="10")
    lala_regular_consuption = models.CharField(max_length=50,default="29")
    lala_trial = models.CharField(max_length=50,default="62")
    lala_total = models.CharField(max_length=50,default="85")


class HysMilkMoms(models.Model):
    class Meta:
        verbose_name_plural = 'HysMilkMoms'
    hys_frequent = models.CharField(max_length=50,default="0")
    hys_regular_consuption = models.CharField(max_length=50,default="0")
    hys_trial = models.CharField(max_length=50,default="0")
    hys_total = models.CharField(max_length=50,default="0")
    alpura_frequent = models.CharField(max_length=50,default="0")
    alpura_regular_consuption = models.CharField(max_length=50,default="0")
    alpura_trial = models.CharField(max_length=50,default="0")
    alpura_total = models.CharField(max_length=50,default="0")
    santa_clara_frequent = models.CharField(max_length=50,default="0")
    santa_clara_regular_consuption = models.CharField(max_length=50,default="0")
    santa_clara_trial = models.CharField(max_length=50,default="0")
    santa_clara_total = models.CharField(max_length=50,default="0")
    lala_frequent = models.CharField(max_length=50,default="0")
    lala_regular_consuption = models.CharField(max_length=50,default="0")
    lala_trial = models.CharField(max_length=50,default="0")
    lala_total = models.CharField(max_length=50,default="0")


class OtherCategories(models.Model):
    class Meta:
        verbose_name_plural = 'OtherCategories'
    consumers_with_acquisitive_capability = models.CharField(default="30,000,000", max_length=50)
    tons = models.CharField(default="157,000", max_length=50)
    pesos = models.CharField(default="62,758,0000,00", max_length=50)
    formal_companies = models.CharField(default="3,700", max_length=50)
    consume_food_from_this_category = models.CharField(max_length=50,default=97)
    from_to = models.CharField(max_length=100, help_text='from 2017 to 2012')
    from_to_value = models.CharField(max_length=50,default="40")
    pesos_year = models.CharField(max_length=50,default="41")
    importing = models.CharField(max_length=50,default="17")
    participation = models.CharField(max_length=50,default="2.3")
    billion_dollars = models.CharField(max_length=50,default="47")
    come_from_percentage = models.CharField(max_length=50,default="99")
    sale_mexican_market_from = models.CharField(max_length=50,default="33")
    sale_mexican_market_to = models.CharField(max_length=50,default="47")
    francia_dls = models.CharField(max_length=50,default="223")
    francia_tons = models.CharField(max_length=50,default="130")
    drink_soda_home = models.CharField(max_length=50,default="0")
    drink_soda_restaurants = models.CharField(max_length=50,default="0")
    drink_soda_gatherings = models.CharField(max_length=50,default="0")
    flavors_cola = models.CharField(max_length=50,default="60")
    flavors_apple = models.CharField(max_length=50,default="17")
    flavors_sangria = models.CharField(max_length=50,default="4")
    flavors_lime = models.CharField(max_length=50,default="7")
    flavors_others = models.CharField(max_length=50,default="12")
    soda_market_coca_cola = models.CharField(max_length=50,default="78")
    soda_market_pepsi = models.CharField(max_length=50,default="12")
    soda_market_pennafiel = models.CharField(max_length=50,default="4")
    soda_market_others = models.CharField(max_length=50,default="6")

