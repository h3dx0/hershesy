from django.apps import AppConfig


class HysCategoriesConfig(AppConfig):
    name = 'hersheys_web.hys_categories'
