from django.contrib import admin
from hersheys_web.hys_categories.models import *

# Register your models here.
admin.site.register(Chocolates)
admin.site.register(BarConsumers)
admin.site.register(BitesConsumers)
admin.site.register(KissesConsumers)
admin.site.register(SpicyCandy)
admin.site.register(SpicyCandySegment)
admin.site.register(Consuption)
admin.site.register(Milk)
admin.site.register(HysMilkKids)
admin.site.register(HysMilkMoms)
admin.site.register(OtherCategories)
admin.site.register(SwotOfHersheys)
