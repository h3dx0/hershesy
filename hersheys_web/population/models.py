from django.db import models


# Create your models here.
class Generals(models.Model):
    class Meta:
        verbose_name_plural = 'Generals'
    population = models.CharField(max_length=100,default=10)
    citizens = models.CharField(max_length=100,default="119.530.753")
    annual_population_growth = models.CharField(max_length=100,default=0)
    women_live_on_avarage = models.CharField(max_length=100,default="6 to 8 years more than men do.")
    young_people = models.CharField(max_length=100,default=30)
    young_people_range = models.CharField(max_length=100,default="15 to 29 years old")
    older_people = models.CharField(max_length=100,default="increase 4%")
    older_people_first_value = models.CharField(max_length=100,default=None)
    older_people_second_value = models.CharField(max_length=100,default=None)
    diseases_year = models.CharField(max_length=100,default="2005")
    death_year = models.CharField(max_length=100,default="12")
    violence_automobile = models.CharField(max_length=100,default="In the last 10 years")
    religion = models.CharField(max_length=100,default="8 out of every 10")
    catholic = models.CharField(max_length=100,default="40% by tradition ")


class SocioEconomicLevel(models.Model):
    class Meta:
        verbose_name_plural = 'SocioEconomicLevel'
    ab = models.CharField(max_length=100,default=6.8)
    e = models.CharField(max_length=100,default=5)
    d = models.CharField(max_length=100,default=21.4)
    dplus = models.CharField(max_length=100,default=18.5)
    cminus = models.CharField(max_length=100,default=17.1)
    c = models.CharField(max_length=100,default=17.1)
    cplus = models.CharField(max_length=100,default=14.2)
    first_year = models.CharField(max_length=100,default="2000")
    second_year = models.CharField(max_length=100,default="2006")
    third_year = models.CharField(max_length=100,default="2012")
    farming_classes1 = models.CharField(max_length=100,default=17)
    farming_classes2 = models.CharField(max_length=100,default=49)
    farming_classes3 = models.CharField(max_length=100,default=34)
    working_classes1 = models.CharField(max_length=100,default=13)
    working_classes2 = models.CharField(max_length=100,default=52)
    working_classes3 = models.CharField(max_length=100,default=36)
    middle_high_classes1 = models.CharField(max_length=100,default=12)
    middle_high_classes2 = models.CharField(max_length=100,default=51)
    middle_high_classes3 = models.CharField(max_length=100,default=37)

    important_directors1 = models.CharField(max_length=100,default=1)
    important_directors2 = models.CharField(max_length=100,default=10)
    important_directors3 = models.CharField(max_length=100,default=5)

    superior_technicians1 = models.CharField(max_length=100,default=5)
    superior_technicians2 = models.CharField(max_length=100,default=2)
    superior_technicians3 = models.CharField(max_length=100,default=6)

    office_workers1 = models.CharField(max_length=100,default=10)
    office_workers2 = models.CharField(max_length=100,default=4)
    office_workers3 = models.CharField(max_length=100,default=8)

    dependents_commerce1 = models.CharField(max_length=100,default=6)
    dependents_commerce2 = models.CharField(max_length=100,default=3)
    dependents_commerce3 = models.CharField(max_length=100,default=7)

    small_bussiness1 = models.CharField(max_length=100,default=6)
    small_bussiness2 = models.CharField(max_length=100,default=8)
    small_bussiness3 = models.CharField(max_length=100,default=3)

    freelancers1 = models.CharField(max_length=100,default=5)
    freelancers2 = models.CharField(max_length=100,default=4)
    freelancers3 = models.CharField(max_length=100,default=4)

    skilled_semiskilled1 = models.CharField(max_length=100,default=5)
    skilled_semiskilled2 = models.CharField(max_length=100,default=5)
    skilled_semiskilled3 = models.CharField(max_length=100,default=8)

    unskilled1 = models.CharField(max_length=100,default=0.5)
    unskilled2 = models.CharField(max_length=100,default=0.4)
    unskilled3 = models.CharField(max_length=100,default=4)

    agricultural1 = models.CharField(max_length=100,default=4)
    agricultural2 = models.CharField(max_length=100,default=0.8)
    agricultural3 = models.CharField(max_length=100,default=5)

    small_agricultural1 = models.CharField(max_length=100,default=8)
    small_agricultural2 = models.CharField(max_length=100,default=0.3)
    small_agricultural3 = models.CharField(max_length=100,default=6)


class HighClassAB(models.Model):
    class Meta:
        verbose_name_plural = 'HighClassAB'
    high_percentaje = models.CharField(max_length=100,default=6.8)
    over = models.CharField(max_length=100,default=80.458)
    average_income = models.CharField(max_length=100,default="80,458.00")
    income_frome_work = models.CharField(max_length=100,default=60)
    renting_a_propety = models.CharField(max_length=100,default=15)
    transfers = models.CharField(max_length=100,default=14)
    rent_estimation = models.CharField(max_length=100,default=11)
    other_income = models.CharField(max_length=100,default=0)
    millions_homes = models.CharField(max_length=100,default=0.4)
    total_monthly = models.CharField(max_length=100,default=34.5)
    what_where_buy = models.CharField(max_length=100,default="143,850.00")
    spend = models.CharField(max_length=100,default="67% in food outside")
    excellent_quality_life = models.CharField(max_length=100,default=6.8)


class UpperMiddleClass(models.Model):
    class Meta:
        verbose_name_plural = 'UpperMiddleClass'
    from_value = models.CharField(max_length=100,default="40,600")
    to_value = models.CharField(max_length=100,default="98,449")
    income = models.CharField(max_length=100,default=0)
    income_work = models.CharField(max_length=100,default=72)
    rent_estimation_middleclass = models.CharField(max_length=100,default=15)
    transfers = models.CharField(max_length=100,default=13)
    renting_property = models.CharField(max_length=100,default=14)
    other_income = models.CharField(max_length=100,default=0)
    millions_homes = models.CharField(max_length=100,default=1.0)
    total_monthly = models.CharField(max_length=100,default=32.2)
    what_where_buy = models.CharField(max_length=100,default=None)
    the_homes_country = models.CharField(max_length=100,default=14)
    their_homes_are = models.CharField(max_length=100,default="more than 200 ")
    upper_middle_class = models.CharField(max_length=100,default=None)
    where_what_years = models.CharField(max_length=100,default="2015-2016")


class TypicalMiddleClassC(models.Model):
    class Meta:
        verbose_name_plural = 'TypicalMiddleClassC'
    middle_class_grew = models.CharField(max_length=100,default=33.8)
    from_to = models.CharField(max_length=100,default="11.8 to 15.8")
    yearly = models.CharField(max_length=100,default="6")
    rent_estimation = models.CharField(max_length=100,default=14)
    transfers = models.CharField(max_length=100,default=16)
    property_rent = models.CharField(max_length=100,default=4)
    income_work = models.CharField(max_length=100,default=67)

    income_average= models.CharField(max_length=100,default="20,062.00")
    other_income = models.CharField(max_length=100,default=0)
    total_monthly = models.CharField(max_length=100,default=22.5)
    millions_homes = models.CharField(max_length=100,default=1.1)

    before_trimester = models.CharField(max_length=100,default="2013 TRIMESTER")
    now_trimester = models.CharField(max_length=100,default="2016 TRIMESTER")
    value_trimester = models.CharField(max_length=100,default=None)
    perfume = models.CharField(max_length=100,default=68)
    labial = models.CharField(max_length=100,default=20)
    soup_serial = models.CharField(max_length=100,default=19)
    young_families = models.CharField(max_length=100,default=17)


class EmergentMiddleClassCMinus(models.Model):
    class Meta:
        verbose_name_plural = 'EmergentMiddleClassCMinus'
    income_work = models.CharField(max_length=100,default=70)
    rent_estimation = models.CharField(max_length=100,default=13)
    transfers = models.CharField(max_length=100,default=15)
    property_rent = models.CharField(max_length=100,default=2)

    income_average = models.CharField(max_length=100,default=14023.00)
    other_income = models.CharField(max_length=100,default=0)
    million_homes = models.CharField(max_length=100,default=1.2)
    total_monthly = models.CharField(max_length=100,default=16.9)
    headed_mothers = models.CharField(max_length=100,default="30% are headed by mothers")


class LowerMiddleClass(models.Model):
    class Meta:
        verbose_name_plural = 'LowerMiddleClass'
    hours_tv = models.CharField(max_length=100,default=None)
    income_work = models.CharField(max_length=100,default=62)
    rent_estimation = models.CharField(max_length=100,default=20)
    transfers = models.CharField(max_length=100,default=17)
    property_rent = models.CharField(max_length=100,default=1)

    income_average = models.CharField(max_length=100,default="6,788.00")
    other_income = models.CharField(max_length=100,default=0)
    million_homes = models.CharField(max_length=100,default=1.6)
    total_monthly = models.CharField(max_length=100,default=10.7)
    econopack= models.CharField(max_length=100,default=None)
    where_what_buy = models.CharField(max_length=100,default=0)
    homes_country = models.CharField(max_length=100,default=18.5)


class MarginalizedClass(models.Model):
    class Meta:
        verbose_name_plural = 'MarginalizedClass'
    income_work = models.CharField(max_length=100,default=61)
    rent_estimation = models.CharField(max_length=100,default="17")
    transfers = models.CharField(max_length=100,default="20")
    property_rent = models.CharField(max_length=100,default= "2")
    income_average = models.CharField(max_length=100,default="0")
    other_income = models.CharField(max_length=100,default="3,355")
    million_homes = models.CharField(max_length=100,default="0.6")
    total_monthly = models.CharField(max_length=100,default="1.9")


class SelDiferencesByRegion(models.Model):
    class Meta:
        verbose_name_plural = 'SelDiferencesByRegion'
    nutritional_poverty = models.CharField(max_length=100,default=3.6)
    equity_poverty = models.CharField(max_length=100,default=27.5)
    middle_class_2002 = models.CharField(max_length=100,default="40% of the wealth")
    to_2016 = models.CharField(max_length=100,default=43)
    guerrero = models.CharField(max_length=100,default="65.2")
    oaxaca = models.CharField(max_length=100,default="66.8")
    chiapas = models.CharField(max_length=100,default="76.2")
    central_region_people = models.CharField(max_length=100,default="90 Million")
    central_gdp = models.CharField(max_length=100,default="GDP Per capita")
    central_millions_usd = models.CharField(max_length=100,default="30,000 USD annually")

#     me quede en la pagina 37 de la escaleta








