# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-25 14:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('population', '0010_auto_20180410_2211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='seldiferencesbyregion',
            name='chiapas',
            field=models.CharField(default='76.2', max_length=50),
        ),
        migrations.AlterField(
            model_name='seldiferencesbyregion',
            name='guerrero',
            field=models.CharField(default='65.2', max_length=50),
        ),
        migrations.AlterField(
            model_name='seldiferencesbyregion',
            name='oaxaca',
            field=models.CharField(default='66.8', max_length=50),
        ),
    ]
