from django.contrib import admin
from hersheys_web.population.models import *

# Register your models here.
admin.site.register(Generals)
admin.site.register(SocioEconomicLevel)
admin.site.register(HighClassAB)
admin.site.register(UpperMiddleClass)
admin.site.register(TypicalMiddleClassC)
admin.site.register(EmergentMiddleClassCMinus)
admin.site.register(LowerMiddleClass)
admin.site.register(MarginalizedClass)
admin.site.register(SelDiferencesByRegion)
