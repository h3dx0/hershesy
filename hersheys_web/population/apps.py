from django.apps import AppConfig


class PopulationConfig(AppConfig):
    name = 'hersheys_web.population'
