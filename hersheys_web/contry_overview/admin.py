from django.contrib import admin
from django.contrib.admin import AdminSite
from hersheys_web.contry_overview.models import *
# Register your models here.


class MyAdminSite(AdminSite):
    site_header = 'Hersheys Presentation administration'

admin.site = MyAdminSite(name='myadmin')
admin.site.register(DiferencesByRegion)
admin.site.register(Goverment)
admin.site.register(EconomicContext)
admin.site.register(WealthDistributionRegion)