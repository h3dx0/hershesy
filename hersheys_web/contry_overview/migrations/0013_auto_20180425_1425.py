# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-25 14:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contry_overview', '0012_auto_20180324_2256'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goverment',
            name='political_party',
            field=models.CharField(choices=[('PRI', 'PRI'), ('PAN', 'PAN'), ('MORENA', 'MORENA')], default='PRI', max_length=100),
        ),
    ]
