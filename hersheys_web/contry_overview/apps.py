from django.apps import AppConfig


class ContryOverviewConfig(AppConfig):
    name = 'hersheys_web.contry_overview'
