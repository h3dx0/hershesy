from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import Transpose
from hersheys_web.utils import PathAndRename

# Create your models here.
path_and_rename_imagen_pdt = PathAndRename("presidente")


class DiferencesByRegion(models.Model):
    population_central_area = models.CharField(max_length=100, default="")
    population_cdmx = models.CharField(max_length=100, default="")
    surface_cdmx = models.CharField(max_length=100,default="")
    metropolitan_area_vdm = models.CharField(max_length=100, default="")
    population_nuevo_leon = models.CharField(max_length=100, default="")
    gdp_monterrey = models.CharField(max_length=100, default="")
    surface_monterrey = models.CharField(max_length=100, default="")
    population_jalisco = models.CharField(max_length=100, default="")
    gdp_jalisco = models.CharField(max_length=100, default="")
    surface_jalisco = models.CharField(max_length=100, default="")
    population_yucatan = models.CharField(max_length=100, default="1,956,000")
    gdp_gdp = models.CharField(max_length=100, default="")
    surface_yucatan = models.CharField(max_length=100, default="")
    rural_inhabitants_yucatan = models.CharField(max_length=100, default="")

    class Meta:
        verbose_name_plural = 'DiferencesByRegion'

PARTIDO_PRESIDENTE = (
    ('PRI', 'PRI'),
    ('PAN', 'PAN'),
    ('MORENA', 'MORENA'),
    ('PRD', 'PRD'),
    ('Movimiento Ciudadano', 'Movimiento Ciudadano'),
    ('PT', 'PT'),
    ('Independiente', 'Independiente'),
    ('Nueva Alianza', 'Nueva Alianza'),
    ('Partido Verde Ecologista ', 'Partido Verde Ecologista '),
    ('Encuentro Social ', 'Encuentro Social'),
)
class Goverment(models.Model):
    text_base_on = models.CharField(max_length=100,default=None)
    year_election = models.IntegerField(default=6)
    text_reelection = models.CharField(max_length=100,default=None)
    chamber_of_diputies = models.IntegerField(default=500)
    senate_of_republic = models.IntegerField(default=32)
    popular_vote_total = models.IntegerField(default=300)
    proportional_representation = models.IntegerField(default=300)
    sentate_members = models.IntegerField(default=96)
    every_two_year = models.IntegerField(default=32)
    president_text = models.CharField(max_length=100, default="Enrique Peña Nieto", null=True, blank=True)
    president_img = ProcessedImageField(max_length=255, blank=True, null=True,
                                 upload_to=path_and_rename_imagen_pdt,
                                 processors=[Transpose()])
    since = models.CharField(max_length=100,default="December 1st 2012.",help_text="December 1st 2012")
    political_party =  models.CharField(max_length=100, default='PRI', choices=PARTIDO_PRESIDENTE)
    next_elections_month = models.CharField(max_length=100, default="")
    next_elections_year = models.CharField(max_length=100, default="")
    next_elections_senate_year = models.CharField(max_length=100,default="")
    next_elections_deputies_year = models.CharField(max_length=100, default="")
    class Meta:
        verbose_name_plural = 'Goverment'


class EconomicContext(models.Model):
    class Meta:
        verbose_name_plural = 'EconomicContext'
    first_year = models.IntegerField(default=2015)
    first_year_percentage = models.CharField(max_length=100, default="2.5")
    second_year = models.IntegerField(default=2016)
    second_year_percentage = models.CharField(max_length=100, default="2.6")

    three_year = models.IntegerField(default=2017)
    three_year_percentage = models.CharField(max_length=100, default="0")
    four_year = models.IntegerField(default=2018)
    four_year_percentage = models.CharField(max_length=100, default="0")
    five_year = models.IntegerField(default=2019)
    five_year_percentage = models.CharField(max_length=100, default="0")
    six_year = models.IntegerField(default=2020)
    six_year_percentage = models.CharField(max_length=100, default="0")
    seven_year = models.IntegerField(default=2021)
    seven_year_percentage = models.CharField(max_length=100, default="0")
    eight_year = models.IntegerField(default=2022)
    eight_year_percentage = models.CharField(max_length=100, default="0")
    nine_year = models.IntegerField(default=2023)
    nine_year_percentage = models.CharField(max_length=100, default="0")
    ten_year = models.IntegerField(default=2024)
    ten_year_percentage = models.CharField(max_length=100, default="0")
    "64"
    first_year_inflation = models.IntegerField(default=2015)
    second_year_inflation = models.IntegerField(default=2015)
    range_percentage = models.CharField(max_length=100, default="2.7% To 3.4%")
    "65"
    price_of_gasolines = models.CharField(max_length=100, default="15-20%")

    inflation_consumer_goods_actual = models.CharField(max_length=100, default="4.9")
    inflation_consumer_goods_one = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_year_one = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_two = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_year_two = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_three = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_year_three = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_four = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_year_four = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_five = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_year_five = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_six = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_year_six = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_seven = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_year_seven = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_eight = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_year_eight = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_nine = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_year_nine = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_ten = models.CharField(max_length=100, default="0")
    inflation_consumer_goods_year_ten = models.CharField(max_length=100, default="0")

    bank_forecast = models.CharField(max_length=100, default="2017 is of 4-6%")
    "66"
    year_reduction = models.IntegerField(default=2016)
    reduction_from = models.CharField(max_length=100,default="from $5,500,000,000 USD")
    reduction_to = models.CharField(max_length=100,default="to $27,900,000,000 USD")
    percentage_gdp = models.CharField(max_length=100,default="(2.7 percent of the GDP)")
    value_increase_money = models.CharField(max_length=100,default=8.8)
    "68"
    fiscal_deficit = models.CharField(max_length=100,default="")
    fiscal_year = models.CharField(max_length=100,default="")
    "69"
    families = models.CharField(max_length=100,default="")
    monthly_wage = models.CharField(max_length=100,default="")
    purchasing_power = models.CharField(max_length=100,default="")
    anual_spending = models.CharField(max_length=100,default="")
    anual_spending_per_person = models.CharField(max_length=100,default="")
    non_alcoholic_food = models.CharField(max_length=100,default="")
    housing_water = models.CharField(max_length=100,default="")
    transportation = models.CharField(max_length=100,default="")
    restaurant = models.CharField(max_length=100,default="")
    health = models.CharField(max_length=100,default="")
    goods_services = models.CharField(max_length=100,default="")
    clothing = models.CharField(max_length=100,default="")
    recreation = models.CharField(max_length=100,default="")
    decoration = models.CharField(max_length=100,default="")
    communication = models.CharField(max_length=100,default="")
    education = models.CharField(max_length=100,default="")
    "70"
    movies = models.CharField(max_length=100,default="")
    concerts = models.CharField(max_length=100,default="")
    tv = models.CharField(max_length=100,default="")
    tv_online = models.CharField(max_length=100,default="")
    music_online = models.CharField(max_length=100,default="")
    smartphone = models.CharField(max_length=100,default="")
    tablet = models.CharField(max_length=100,default="")
    ""
    depreciation_peso_first_year = models.CharField(max_length=100,default=None)
    depreciation_peso_second_year = models.CharField(max_length=100,default=None)
    depreciation_peso_first_value = models.CharField(max_length=100,default="")
    depreciation_peso_second_value = models.CharField(max_length=100,default="")
    inflation = models.CharField(max_length=100,default="")
    percentage_range = models.CharField(max_length=100,default=None, help_text="Example: 15% - 20%")
    # year_external = models.IntegerField(default="")
    # from_value = models.CharField(max_length=100,default="")
    # to_value = models.CharField(max_length=100,default="")
    # increase_percentage = models.CharField(max_length=100,default="")
    # percent_gdp = models.CharField(max_length=100,default="")

    "71"
    gdp_2017 = models.CharField(max_length=100,default="987.30")
    gdp_2018 = models.CharField(max_length=100,default="1.00")
    gdp_2019 = models.CharField(max_length=100,default="987.30")
    gdp_2020 = models.CharField(max_length=100,default="1.00")
    gdp_anual_2017 = models.CharField(max_length=100,default="1.7")
    gdp_anual_2018 = models.CharField(max_length=100,default="2.0")
    gdp_anual_2019 = models.CharField(max_length=100,default="1.7")
    gdp_anual_2020 = models.CharField(max_length=100,default="2.0")
    gdp_per_capita_2017 = models.CharField(max_length=100,default="7")
    gdp_per_capita_2018 = models.CharField(max_length=100,default="8")
    gdp_per_capita_2019 = models.CharField(max_length=100,default="7")
    gdp_per_capita_2020 = models.CharField(max_length=100,default="8")
    public_finance_2017 = models.CharField(max_length=100,default="-2.6")
    public_finance_2018 = models.CharField(max_length=100,default="-2.3")
    public_finance_2019 = models.CharField(max_length=100,default="-2.6")
    public_finance_2020 = models.CharField(max_length=100,default="-2.3")
    state_debt_2017 = models.CharField(max_length=100,default="57.2")
    state_debt_2018 = models.CharField(max_length=100,default="56.8")
    state_debt_2019 = models.CharField(max_length=100,default="57.2")
    state_debt_2020 = models.CharField(max_length=100,default="56.8")
    inflation_rate_2017 = models.CharField(max_length=100,default="4.8")
    inflation_rate_2018 = models.CharField(max_length=100,default="3.2")
    inflation_rate_2019 = models.CharField(max_length=100,default="4.8")
    inflation_rate_2020 = models.CharField(max_length=100,default="3.2")
    unemployment_2017 = models.CharField(max_length=100,default="4.4")
    unemployment_2018 = models.CharField(max_length=100,default="4.4")
    unemployment_2019 = models.CharField(max_length=100,default="4.4")
    unemployment_2020 = models.CharField(max_length=100,default="4.4")
    current_transaction_balance_usd_2017 = models.CharField(max_length=100,default="-24.47")
    current_transaction_balance_usd_2018 = models.CharField(max_length=100,default="-27.48")
    current_transaction_balance_usd_2019 = models.CharField(max_length=100,default="-24.47")
    current_transaction_balance_usd_2020 = models.CharField(max_length=100,default="-27.48")
    current_transaction_balance_gdp_2017 = models.CharField(max_length=100,default="")
    current_transaction_balance_gdp_2018 = models.CharField(max_length=100,default="")
    current_transaction_balance_gdp_2019 = models.CharField(max_length=100,default="")
    current_transaction_balance_gdp_2020 = models.CharField(max_length=100,default="")
    "72"
    agriculture_gdp = models.CharField(max_length=100,default=3.5)
    gas_oil = models.CharField(max_length=100,default="5 BIGGEST")
    "73"
    space_ind_companies = models.CharField(max_length=100,default="190 companies")
    space_ind_employ = models.CharField(max_length=100,default="30,000 EMPLOYEES")
    automotive_rank = models.CharField(max_length=100,default="TOP TEN")
    "74"
    call_center = models.CharField(max_length=100,default="25% (third of the GDP)")
    service_gdp = models.CharField(max_length=100,default=60)
    service_active_population = models.CharField(max_length=100,default=60)
    construction = models.CharField(max_length=100,default="Has recovered since 2010 due to strong investment in real estate")
    "75"
    automobile_export = models.CharField(max_length=100,default="1st")
    remittances = models.CharField(max_length=100,default=None)
    american_construction = models.CharField(max_length=100,default=80)
    american_construction_start_year = models.IntegerField(default=200)
    american_construction_end_year = models.IntegerField(default=2016)
    american_construction_start_value = models.CharField(max_length=100,default="6.572.700.000")
    american_construction_end_value = models.CharField(max_length=100,default="26.972.400.000")
    "76"
    usa_economic_evol = models.CharField(max_length=100,default="more jobs")
    usa_year_incomes = models.IntegerField(default=2017)
    usa_year_value = models.IntegerField(default=7)
    usa_year_text = models.CharField(max_length=100,default="MORE THAN IN 2016 \n (FIRST TRIMESTER)")
    "77"
    min_wage_peso = models.CharField(max_length=100,default="73.34")
    total_wage_peso = models.CharField(max_length=100,default="1,725.96")
    food_basket = models.CharField(max_length=100,default="1,346.46")
    no_food_basket = models.CharField(max_length=100,default="1,361")
    total_food_no_food = models.CharField(max_length=100,default="2,707.46")
    "78"
    lowest_min_wage_text = models.CharField(max_length=100,default="lowest minimum wage")
    mexico_text = models.CharField(max_length=100,default="Mexico: Less than 1 usd an hour \n less than in chile an estonia")
    works_mexico_hour = models.CharField(max_length=100,default="2,226")
    works_usd_year_mexico = models.CharField(max_length=100,default=None)
    australia = models.CharField(max_length=100,default="")
    luxemburgo = models.CharField(max_length=100,default="")
    france = models.CharField(max_length=100,default="")
    works_hrs_year_countries = models.CharField(max_length=100,default="")
    works_usd_year_countries = models.CharField(max_length=100,default="")
    "79"
    hoard = models.CharField(max_length=100,default="")
    live_poverty = models.CharField(max_length=100,default="")
    "81"
    monthly_rent = models.CharField(max_length=100,default="8,000")
    transport_cost = models.CharField(max_length=100,default="260,000")
    public_bus = models.CharField(max_length=100,default="7.5")
    nutrition = models.CharField(max_length=100,default="16")
    hershey = models.CharField(max_length=100,default="14")


class WealthDistributionRegion(models.Model):
    class Meta:
        verbose_name_plural = 'WealthDistributionRegion'
    wealth_distribution_san_nicolas = models.CharField(max_length=100,default="25.636")
    wealth_distribution_first_year = models.CharField(max_length=100,default="2015")
    wealth_distribution_second_year = models.CharField(max_length=100,default="2016")
    wealth_distribution_money = models.CharField(max_length=100,default="0")
    wealth_distribution_people_not_food = models.CharField(max_length=100,default="10.76")
    wealth_distribution_state = models.CharField(max_length=100,default="8")
    new_jobs = models.CharField(max_length=100,default="14000")

    # me quede en la slide 66