from django.apps import AppConfig


class EatingHabitsConfig(AppConfig):
    name = 'hersheys_web.eating_habits'
