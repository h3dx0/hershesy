from django.conf.urls import url
from hersheys_web.api import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
]
