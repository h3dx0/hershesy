from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'hersheys_web.api'
