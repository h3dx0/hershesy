from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
import uuid


class Usuario(AbstractUser):
    objects = UserManager()
    apem = models.CharField('Apellido Materno', max_length=30, null=True, blank=True)
    nombre_completo = models.CharField(max_length=100, null=True, blank=True, editable=False)

    def get_full_name(self):
        full_name = '%s %s %s' % (self.first_name, self.apem, self.last_name)
        return full_name.strip()
