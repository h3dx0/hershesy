from rest_framework import serializers
from hersheys_web.contry_overview.models import *
from hersheys_web.eating_habits.models import *
from hersheys_web.population.models import *
from hersheys_web.hys_categories.models import *


class DiferencesByRegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiferencesByRegion
        fields = '__all__'