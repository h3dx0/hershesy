from django.db import models


class SlideSourceData(models.Model):
    slide_number = models.IntegerField()
    slide_source_text = models.TextField(max_length=500)

    def __str__(self):
        return "{}-{}".format(self.slide_number, self.slide_source_text)