from django.apps import AppConfig


class SourceConfigConfig(AppConfig):
    name = 'hersheys_web.source_config'
