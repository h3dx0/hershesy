from django.contrib import admin
from hersheys_web.source_config.models import SlideSourceData
# Register your models here.
admin.site.register(SlideSourceData)