from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from hersheys_web.contry_overview.models import *
from hersheys_web.population.models import *
from hersheys_web.hys_categories.models import *
from hersheys_web.source_config.models import *
from django.contrib.auth.decorators import login_required


# Create your views here.

@login_required
def index(request, pagina):
    datos = None
    num_pagina = pagina[:-5]

    if 15 < int(num_pagina) < 51:
        datos = DiferencesByRegion.objects.all().first()

    if 52 < int(num_pagina) < 61:
        datos = Goverment.objects.all().first()

    if 61 < int(num_pagina) < 82:
        datos = EconomicContext.objects.all().first()

    if 82 < int(num_pagina) < 93:
        datos = WealthDistributionRegion.objects.all().first()

    if 93 < int(num_pagina) < 100:
        datos = Generals.objects.all().first()

    if 100 < int(num_pagina) < 108:
        datos = SocioEconomicLevel.objects.all().first()

    if 108 < int(num_pagina) < 117:
        datos = HighClassAB.objects.all().first()

    if 117 < int(num_pagina) < 126:
        datos = UpperMiddleClass.objects.all().first()

    if 126 < int(num_pagina) < 138:
        datos = TypicalMiddleClassC.objects.all().first()

    if 139 < int(num_pagina) < 142:
        datos = EmergentMiddleClassCMinus.objects.all().first()

    if 142 < int(num_pagina) < 155:
        datos = LowerMiddleClass.objects.all().first()

    if 155 < int(num_pagina) < 159:
        datos = MarginalizedClass.objects.all().first()

    if 159 <= int(num_pagina) < 165:
        datos = SelDiferencesByRegion.objects.all().first()

    if 212 < int(num_pagina) < 225:
        datos = Chocolates.objects.all().first()

    if 224 < int(num_pagina) < 226:
        datos = BarConsumers.objects.all().first()

    if int(num_pagina) == 226:
        datos = BitesConsumers.objects.all().first()

    if int(num_pagina) == 227:
        datos = KissesConsumers.objects.all().first()

    if int(num_pagina) == 228:
        datos = SwotOfHersheys.objects.all().first()

    if 230 < int(num_pagina) < 241:
        datos = SpicyCandy.objects.all().first()

    if int(num_pagina) == 241:
        datos = SpicyCandySegment.objects.all().first()

    if int(num_pagina) == 242:
        datos = Consuption.objects.all().first()
        print(datos)

    if 244 < int(num_pagina) < 251:
        datos = Milk.objects.all().first()

    if int(num_pagina) == 252:
        datos = HysMilkKids.objects.all().first()

    if 252 < int(num_pagina) < 262:
        datos = OtherCategories.objects.all().first()
    try:
        source = SlideSourceData.objects.get(slide_number=num_pagina)
    except Exception :
        source = None
    return render(request, pagina, context={'datos': datos, 'source': source})


def source(request):
    for slide in range(1, 262):
        slide_source = SlideSourceData()
        slide_source.slide_number = slide
        slide_source.slide_source_text = ""
        slide_source.save()
        print(slide)
    return HttpResponseRedirect('/web/1.html')