from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'hersheys_web.main'
