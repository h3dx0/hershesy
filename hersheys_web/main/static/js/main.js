window.addEventListener('wheel', function (e) {
    let currentSlide = document.body.getAttribute('data-slide');
    let next = e.deltaY < 0 ? parseInt(currentSlide) - 1 : parseInt(currentSlide) + 1;
    next = next == 0 ? 1 : next;
    window.location.href = next + '.html';
}, false);

window.addEventListener("keydown", function (event) {
    let currentSlide = document.body.getAttribute('data-slide');
    let next = event.code == 'ArrowDown' || event.code == 'ArrowRight' ? parseInt(currentSlide) + 1 : event.code == 'ArrowUp' || event.code == 'ArrowLeft' ? parseInt(currentSlide) - 1 : currentSlide;
    next = next == 0 ? 1 : next;
    if (currentSlide != next) {
        window.location.href = next + '.html';
    }
}, false);

/*
 ** Charts
 */
var chartPoints = document.querySelectorAll('span.chart-point');
var chartElem = document.querySelector('.chart-virtual-area');
if (chartPoints != null && chartElem != null) {
    var chart = chartElem.getAttribute("data-graph");
    for (var chartPoint of chartPoints) {
        var xPos = chartPoint.getAttribute("data-x");
        var yPos = chartPoint.getAttribute("data-y");
        switch (chart) {

            case "A":
                var xCoord = ( ( xPos - 2015 ) * 15 ) - 2;
                var yCoord = ( ( yPos - 2.1 ) * 72.5 ) - 2;
                chartPoint.style.left = xCoord + "%";
                chartPoint.style.bottom = yCoord + "%";
                break;

            case "B":
                var xCoord = ( xPos - 2016 ) * 17.5;
                var yCoord = ( ( ( yPos - 2 ) * 10 ) * 1.6 ) + 4;
                chartPoint.style.left = xCoord + "%";
                chartPoint.style.bottom = yCoord + "%";
                break;
        }
    }
}

/*
 ** Choco Bars
 */
var chocoBarsEl = document.querySelectorAll('.choco-bars .bar');
if (chocoBarsEl != null) {
    for (var chocoBar of chocoBarsEl) {
        var valor = parseFloat(chocoBar.getAttribute('data-val'));
        chocoBar.style.height = valor * 30 + 'px';
    }
}

/*
 ** Horizontal Bars
 */
var horizBarsEl = document.querySelectorAll('.horizontal-bars .bar');
if (horizBarsEl != null) {
    for (var horizBar of horizBarsEl) {
        var valor = parseFloat(horizBar.getAttribute('data-val'));
        horizBar.style.width = valor + '%';
    }
}

/*
 ** Menu
 */
var menuLink = function (e) {
    //e.preventDefault();
    var target = e.target;
    if (!target.classList.contains('active')) {
        if (target.parentNode.classList.contains('menu-parent')) {
            e.preventDefault();
            var menuItems = document.querySelectorAll('.app-drawer ul li a');
            for (var menuItem of menuItems) {
                menuItem.parentNode.classList.remove('current');
                menuItem.classList.remove('active');
            }
            target.classList.add('active');
            target.parentNode.classList.add('current');
        }
    }
}

var menuItemEl = document.querySelectorAll('.app-drawer ul li a');
if (menuItemEl != null) {
    for (var menuItem of menuItemEl) {
        menuItem.addEventListener('click', menuLink, false);
    }
}

