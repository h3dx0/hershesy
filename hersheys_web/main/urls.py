from django.conf.urls import url
from hersheys_web.main import views


urlpatterns = [
    url(r'^web/(?P<pagina>\w+.html)$', views.index, name='index'),
    url(r'^source$', views.source, name='source'),
]
